Dynare Registry
===============

Julia registry for Dynare packages. It is maintained with
[LocalRegistry](https://github.com/GunnarFarneback/LocalRegistry.jl).

You can configure this registry in your Julia installation with:
```
using Pkg
pkg"registry add https://git.dynare.org/DynareJulia/DynareRegistry.git"
```
